import RomanNumeralGenerator from './../src';

describe('RomanNumberGenerator', () => {
    const generator = new RomanNumeralGenerator();

    it('should return I when passed 1', () => {
        const expected = 'I';
        const actual = generator.generate(1);
        expect(actual).toBe(expected);
    });

    it('should return II when passed 2', () => {
        const expected = 'II';
        const actual = generator.generate(2);
        expect(actual).toBe(expected);
    });

    it('should return correct values for numbers 1 to 10', () => {
        const expectedValues = [
            'I',
            'II',
            'III',
            'IV',
            'V',
            'VI',
            'VII',
            'VIII',
            'IX',
            'X'
        ];

        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].forEach((n, i) => {
            const actual = generator.generate(n);
            expect(actual).toBe(expectedValues[i]);
        });
    });

    it('should return correct values for 11', () => {
        const expected = 'XI';

        const actual = generator.generate(11);

        expect(actual).toBe(expected);
    });

    it('should correctly convert numbers to roman numerals', () => {
        const expectedValues = [
            'XLIX',
            'CCCLXVIII',
            'MCMXXIV',
            'XXXVII',
            'DCCCLXXXVIII',
            'CDLXXXVII'
        ];

        [49, 368, 1924, 37, 888, 487].forEach((n, i) => {
            const actual = generator.generate(n);
            expect(actual).toBe(expectedValues[i]);
        });
    });

    it('throws if you pass a number greater than 3999 or less than zero', () => {
        const message =
            'RomanNumeralGenerator only converts numbers between 1 and 3999';

        expect(() => generator.generate(0)).toThrow(message);
        expect(() => generator.generate(4000)).toThrow(message);
    });
});
