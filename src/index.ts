export default class RomanNumeralGenerator {
    // Lookup table of the different unique symbols present between 1 - 1000
    static symbols = [['I', 'V', 'X'], ['X', 'L', 'C'], ['C', 'D', 'M'], ['M']];

    generate(n: number): string {
        // reject values over 3999 or less than 0
        if (n <= 0 || n > 3999) {
            throw new Error(
                'RomanNumeralGenerator only converts numbers between 1 and 3999'
            );
        }

        // create an array of the different digits present in the number
        // reverse it so that each index represents an ascending power of 10
        const digits = n
            .toString()
            .split('')
            .reverse()
            .map(i => parseInt(i, 10));

        // pass the digit and corresponding array of symbols for that power of 10
        // into convert number
        return digits.reduce(
            (carry, value, index) =>
                `${(<any>this.convertNumber)(
                    value,
                    ...RomanNumeralGenerator.symbols[index]
                )}${carry}`,
            ''
        );
    }

    createSequence(n: number, symbol: string, baseSymbol: string = '') {
        // a for-loop would've sufficed here but wanted to use fancy Array.from!
        return Array.from(Array(n)).reduce(
            carry => `${carry}${symbol}`,
            baseSymbol
        );
    }

    convertNumber(
        n: number,
        single: string,
        five?: string,
        ten?: string
    ): string {
        switch (n) {
            case 0:
                return '';
            case 1:
            case 2:
            case 3:
                return this.createSequence(n, single);
            case 4:
                return `${single}${five}`;
            case 5:
                return five || '';
            case 6:
            case 7:
            case 8:
                return this.createSequence(n - 5, single, five);
            case 9:
                return `${single}${ten}`;
            default:
                return '';
        }
    }
}
